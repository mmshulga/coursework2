package MainPackage;

import Curves.CurvePoint;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * Represents the signature.
 */
public class Signature implements Serializable {
    /**
     * The r*k*G.
     */
    private final CurvePoint point;

    /**
     * The correspnding number for the given signature.
     */
    private final BigInteger d;

    @Override
    public String toString() {
        return "Подпись\n" +
                "Точка = " + point.toString() +
                "\nd = " + d;
    }

    /**
     * Gets the r*k*G.
     * @return the point.
     */
    public CurvePoint getPoint() {
        return point;
    }

    /**
     * Gets the number.
     * @return the d.
     */
    public BigInteger getD() {
        return d;
    }

    /**
     * Constructs the signature using point and the corresponding number.
     * @param point the point with which file was signed.
     * @param d the number with which file was signed.
     */
    public Signature(CurvePoint point, BigInteger d) {
        this.point = point;
        this.d = d;
    }

}
