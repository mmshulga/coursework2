package MainPackage;

import Curves.CurvePoint;
import Curves.EllipticCurve;
import Curves.EllipticCurve4;
import Fields.GaloisField;
import Keys.PrivateKey;
import Keys.PublicKey;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Random;

/**
 * The given class implements the logic for signature and signature verification processes.
 */
public class Crypter {

    /**
     * Field for the curve.
     */
    private static GaloisField field = new GaloisField(
            new BigInteger("6277101735386680763835789423207666416083908700390324961279"));

    /**
     * Params for the curve.
     */
    private static BigInteger
            a = new BigInteger("6277101735386680763835789423207666416083908700390324961276"),
            b = new BigInteger("2455155546008943817740293915197451784769108058161191238065");

    /**
     * Generator the curve.
     */
    private static CurvePoint G = new CurvePoint(
            new BigInteger("602046282375688656758213480587526111916698976636884684818"),
            new BigInteger("174050332293622031404857552280219410364023488927386650641"),
            field);

    /**
     * The order of the generator.
     */
    private static BigInteger n = new BigInteger("6277101735386680763835789423176059013767194773182842284081");

    /**
     * The curve.
     */
    private static EllipticCurve curve = new EllipticCurve4(a, b, field);

    /**
     * Sets up a new list of parameters used in calculations.
     * @param field the new field.
     * @param a the new a-coefficient in equation of the curve.
     * @param b the new b-coefficient in equation of the curve
     * @param G the new generator for the group.
     * @param n the new order of the generator.
     */
    public static void setUp(GaloisField field, BigInteger a, BigInteger b, CurvePoint G, BigInteger n) {
        Crypter.field = field;
        Crypter.a = a;
        Crypter.b = b;
        Crypter.curve = new EllipticCurve4(a, b, field);
        Crypter.G = G;
        Crypter.n = n;
    }

    /**
     * Generates public key from the private key.
     * @param privateKey helps to generate the public key.
     * @return generated public key.
     */
    public static PublicKey generatePublicKey(PrivateKey privateKey) {

        if (privateKey == null) {
            privateKey = generatePrivateKey();
        }

        CurvePoint Y = curve.multiplyPoint(G, privateKey.getKey());
        return new PublicKey(curve, G, Y, n);
    }

    /**
     * Generates the public key.
     * @return private key.
     */
    public static PrivateKey generatePrivateKey() {
        Random rnd = new Random();
        BigInteger key;

        do {
            key = new BigInteger(n.bitCount(), rnd);
        }
        while (key.compareTo(BigInteger.ZERO) <= 0);

        return new PrivateKey(key);
    }

    /**
     * Signs the given message using keys.
     * @param message the message to sign
     * @param publicKey public key using which message is signed.
     * @param privateKey private key using which message is signed.
     * @return the signature.
     */
    public static Signature sign(StringBuilder message, PublicKey publicKey, PrivateKey privateKey) {
        BigInteger hashedMessage = hashFunction(message);

        Random rnd = new Random();
        BigInteger r,
                inversed_r = null;

        do {
            r = new BigInteger(publicKey.getN().bitCount(), rnd);
            if (r.compareTo(BigInteger.ZERO) == 0) continue;
            inversed_r = modInv(r, publicKey.getN());
        } while(inversed_r == null);

        CurvePoint C = publicKey.getCurve().multiplyPoint(publicKey.getGenerator(), r);

        BigInteger d = inversed_r.multiply(hashedMessage.subtract(privateKey.getKey().multiply(hashFunction(C))));
        d = d.mod(publicKey.getN());

        return new Signature(C, d);

    }

    /**
     * Checks if the sign is correct.
     * @param sign the sign to be checked.
     * @param publicKey the public key of the signer.
     * @param text the text to be compared with the sign.
     * @return the result of the comparison.
     */
    public static boolean checkSign(Signature sign, PublicKey publicKey, StringBuilder text) {
        CurvePoint Y = publicKey.getPoint();
        CurvePoint C = sign.getPoint();

        BigInteger hashedC = hashFunction(C);

        CurvePoint U = publicKey.getCurve().multiplyPoint(Y, hashedC);
        U = publicKey.getCurve().sumPoints(U, publicKey.getCurve().multiplyPoint(C, sign.getD()));

        BigInteger hashedMessage = hashFunction(text);

        CurvePoint V = publicKey.getCurve().multiplyPoint(publicKey.getGenerator(), hashedMessage);

        return U.equals(V);
    }

    /**
     * Hashes the given message. Uses standard SHA-256 hash-function.
     * @param message the message to be hashed.
     * @return the result of the hash function over the given message.
     */
    private static BigInteger hashFunction(StringBuilder message) {
        BigInteger result = null;

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            String text = message.toString();

            md.update(text.getBytes("UTF-32")); // Change this to "UTF-16" if needed
            byte[] digest = md.digest();
            result = new BigInteger(digest);
            if (result.compareTo(BigInteger.ZERO) < 0) {
                result = result.negate();
            }
        }
        catch (Exception ex) {
            System.out.println("Something went wrong :(");
        }

        return result;
    }

    /**
     * Hashes the given point. Uses concatenation.
     * @param point the point to be hashed.
     * @return the result of concatenation.
     */
    private static BigInteger hashFunction(CurvePoint point) {
        String x = point.getX().toString();
        String y = point.getY().toString();

        return new BigInteger(x + y);
    }

    /**
     * Gets the inversed element in a finite field over n.
     * @param value the element for which the inversed element is calculated.
     * @param n the order of a finite field.
     * @return the inversed element or null if not possible.
     */
    private static BigInteger modInv(BigInteger value, BigInteger n) {

        // Set up all initial values.
        BigInteger b0 = value;
        BigInteger n0 = n;
        BigInteger t0 = BigInteger.ZERO;
        BigInteger t = BigInteger.ONE;
        BigInteger q = n.divide(b0);
        BigInteger r = n0.subtract(q.multiply(b0));

        // Loop while the remainder in the successive divisions is positive.
        while (r.compareTo(BigInteger.ZERO) > 0) {

            // Simulates the subtraction step in the Extended Euclidean Alg.
            BigInteger temp = t0.subtract(q.multiply(t));

            // Adjust temp if it's negative.
            if (temp.compareTo(BigInteger.ZERO) > 0)
                temp = temp.mod(n);
            if (temp.compareTo(BigInteger.ZERO) < 0)
                temp = n.subtract(temp.negate().mod(n));

            // Update each variable as necessary.
            t0 = t;
            t = temp;
            n0 = b0;
            b0 = r;
            q = n0.divide(b0);
            r = n0.subtract(q.multiply(b0));
        }

        // No inverse!
        if (!b0.equals(BigInteger.ONE))
            return null;

            // Here's the inverse!
        else
            return t.mod(n);
    }
}
