package forms;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("wowsuchcrypto");
        primaryStage.setScene(new Scene(root, 600, 300));
        primaryStage.setResizable(false);
        
        primaryStage.setOnCloseRequest(e -> Platform.exit());
        primaryStage.show();

    }


    public static void main(String[] args) {
        launch(args);
    }
}
