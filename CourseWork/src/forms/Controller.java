package forms;

import Keys.PrivateKey;
import Keys.PublicKey;
import MainPackage.Crypter;
import MainPackage.Signature;
import javafx.scene.control.Button;
import javafx.stage.FileChooser;
import org.controlsfx.dialog.Dialogs;

import java.io.*;
import java.nio.file.Files;

/**
 * Controller class for the JavaFX app.
 */
public class Controller {
    public Button lookPublicKeyButton;
    public Button lookSignButton;
    public Button signerButton;
    public Button checkSignButton;
    public Button generateKeysButton;
    public Button lookPrivateKeyButton;


    /**
     * Implements the functionality for a Generate Keys button.
     */
    public void generateKeys() {

        PrivateKey privateKey;
        PublicKey publicKey;

        try {
            privateKey = Crypter.generatePrivateKey();

            if (! savePrivateKey(privateKey)) return;

            publicKey = Crypter.generatePublicKey(privateKey);
            if (! savePublicKey(publicKey)) return;

            showMessage("Ключи успешно сгенерированы!");
        }
        catch (Exception ex) {
            showErrorMessage();
            ex.printStackTrace();
        }
    }

    /**
     * Implements the functionality for a Check Sign button.
     */
    public void checkSign() {
        Signature signature;
        StringBuilder text;
        PublicKey publicKey;

        try {
            text = openTxtFile();
            if (text == null) {
                return;
            }

            publicKey = openPublicKey();
            if (publicKey == null) {
                return;
            }

            signature = openSignature();
            if (signature == null) {
                return;
            }

            boolean result = Crypter.checkSign(signature, publicKey, text);
            if (result) {
                showMessage("Подпись принята!");

            }
            else {
                showMessage("Подпись отклонена!");
            }
        }
        catch (Exception ex) {
            showErrorMessage();
            ex.printStackTrace();
        }
    }

    /**
     * Implements the functionality for a Sign Document button.
     */
    public void sign() {
        Signature signature;
        StringBuilder text;
        PublicKey publicKey;
        PrivateKey privateKey;

        try {
            text = openTxtFile();
            if (text == null) {
                return;
            }

            publicKey = openPublicKey();
            if (publicKey == null) {
                return;
            }

            privateKey = openPrivateKey();
            if (privateKey == null) {
                return;
            }

            signature = Crypter.sign(text, publicKey, privateKey);
            if (signature == null) {
                return;
            }

            if (! saveSignature(signature)) {
                return;
            }

            showMessage("Документ был успешно подписан!");

        }
        catch (Exception ex) {
            showErrorMessage();
            ex.printStackTrace();
        }
    }

    /**
     * Implements the functionality for a Look Public Key button.
     */
    public void lookPublicKey() {
        try {
            PublicKey publicKey = openPublicKey();
            if (publicKey == null) {
                return;
            }

            showMessage(publicKey.toString());
        }
        catch (Exception ex) {
            showErrorMessage();
            ex.printStackTrace();
        }
    }

    /**
     * Implements the functionality for a Look Private Key button.
     */
    public void lookPrivateKey() {
        try {
            PrivateKey privateKey = openPrivateKey();
            if (privateKey == null) {
                return;
            }

            showMessage(privateKey.toString());
        }
        catch (Exception ex) {
            showErrorMessage();
            ex.printStackTrace();
        }
    }

    /**
     * Implements the functionality for a Look Sign button.
     */
    public void lookSign() {
        try {
            Signature signature = openSignature();

            if (signature == null) {
                return;
            }

            showMessage(signature.toString());
        }
        catch (Exception ex) {
            showErrorMessage();
            ex.printStackTrace();
        }
    }

    /**
     * Displays the message when an error occured.
     */
    private void showErrorMessage() {
        Dialogs.create()
                .owner(generateKeysButton.getScene().getWindow())
                .title(null)
                .masthead(null)
                .message("Произошла ошибка! Пожалуйста, повторите!")
                .showError();
    }

    /**
     * Displays the message when an action cancelled.
     */
    private void showCancelledMessage() {
        Dialogs.create()
                .owner(generateKeysButton.getScene().getWindow())
                .title(null)
                .masthead(null)
                .message("Действие отменено пользователем!")
                .showInformation();
    }

    /**
     * Displays the given message on the screen.
     * @param message the text to display.
     */
    private void showMessage(String message) {
        Dialogs.create()
                .owner(generateKeysButton.getScene().getWindow())
                .title(null)
                .masthead(null)
                .message(message)
                .showInformation();
    }

    /**
     * Saves the private key in a selected file in serialized form.
     * @param privateKey the private key to be saved.
     * @return the success of the saving.
     */
    private boolean savePrivateKey(PrivateKey privateKey) {
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setInitialFileName("Закрытый ключ.privatekey");
            fileChooser.setTitle("Сохраните закрытый ключ");
            File file = fileChooser.showSaveDialog(generateKeysButton.getScene().getWindow());

            if (file == null) {
                showCancelledMessage();
                return false;
            }

            if (! file.getName().endsWith(".privatekey")) {
                file = new File(file.getPath() + ".privatekey");
            }

            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(privateKey);
            oos.flush();
            oos.close();
            return true;
        }
        catch (Exception ex) {
            showErrorMessage();
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * Saves the public key in a selected file in serialized form.
     * @param publicKey the public key to be saved.
     * @return the success of the saving.
     */
    private boolean savePublicKey(PublicKey publicKey) {
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Сохраните открытый ключ");
            fileChooser.setInitialFileName("Открытый ключ.publickey");
            File file = fileChooser.showSaveDialog(generateKeysButton.getScene().getWindow());

            if (file == null) {
                showCancelledMessage();
                return false;
            }

            if (! file.getName().endsWith(".publickey")) {
                file = new File(file.getPath() + ".publickey");
            }

            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(publicKey);
            oos.flush();
            oos.close();

            return true;
        }
        catch (Exception ex) {
            showErrorMessage();
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * Saves the signature in a selected file in serialized form.
     * @param signature the signature to be saved.
     * @return the success of the saving.
     */
    private boolean saveSignature(Signature signature) {
        try {
            FileChooser fileChooser = new FileChooser();

            FileChooser.ExtensionFilter extFilter =
                    new FileChooser.ExtensionFilter("Подпись (*.signature)", "*.signature");
            fileChooser.getExtensionFilters().clear();
            fileChooser.getExtensionFilters().add(extFilter);

            fileChooser.setTitle("Сохраните подпись");
            fileChooser.setInitialFileName("подпись.signature");

            File file = fileChooser.showSaveDialog(generateKeysButton.getScene().getWindow());

            if (file == null) {
                showCancelledMessage();
                return false;
            }

            if (! file.getName().endsWith(".signature")) {
                file = new File(file.getPath() + ".signature");
            }

            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(signature);
            oos.flush();
            oos.close();

            return true;
        }
        catch (Exception ex) {
            showErrorMessage();
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * Opens and reads the selected txt file.
     * @return the text in a file or null if an error occured.
     */
    private StringBuilder openTxtFile() {
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Выберите текстовый файл");

            FileChooser.ExtensionFilter extFilter =
                    new FileChooser.ExtensionFilter("Текстовые файлы (*.txt)", "*.txt");
            fileChooser.getExtensionFilters().clear();
            fileChooser.getExtensionFilters().add(extFilter);

            File file = fileChooser.showOpenDialog(generateKeysButton.getScene().getWindow());

            if (file == null) {
                return null;
            }

            return new StringBuilder(new String(Files.readAllBytes(file.toPath())));
        }
        catch (Exception ex) {
            ex.printStackTrace();
            showErrorMessage();
            return null;
        }
    }

    /**
     * Opens the selected file and deserializes the public key in it.
     * @return the public key or null if an error occured.
     */
    private PublicKey openPublicKey() {
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Выберите открытый ключ");

            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Открытые ключи (*.publickey)", "*.publickey");
            fileChooser.getExtensionFilters().clear();
            fileChooser.getExtensionFilters().add(extFilter);

            File file = fileChooser.showOpenDialog(generateKeysButton.getScene().getWindow());

            if (file == null) {
                showCancelledMessage();
                return null;
            }

            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream oin = new ObjectInputStream(fis);
            PublicKey publicKey = (PublicKey) oin.readObject();
            fis.close();
            oin.close();
            return publicKey;
        }
        catch (Exception ex) {
            showErrorMessage();
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Opens the selected file and deserializes the private key in it.
     * @return the private key or null if an error occured.
     */
    private PrivateKey openPrivateKey() {
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Выберите закрытый ключ");

            FileChooser.ExtensionFilter extFilter =
                    new FileChooser.ExtensionFilter("Закрытые ключи (*.privatekey)", "*.privatekey");
            fileChooser.getExtensionFilters().clear();
            fileChooser.getExtensionFilters().add(extFilter);

            File file = fileChooser.showOpenDialog(generateKeysButton.getScene().getWindow());

            if (file == null) {
                showCancelledMessage();
                return null;
            }

            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream oin = new ObjectInputStream(fis);
            PrivateKey privateKey = (PrivateKey) oin.readObject();
            fis.close();
            oin.close();
            return privateKey;
        }
        catch (Exception ex) {
            showErrorMessage();
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Opens the selected file and deserializes the signature in it.
     * @return the signature or null if an error occured.
     */
    private Signature openSignature() {
        try {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Выберите файл с подписью");

            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Подпись (*.signature)", "*.signature");
            fileChooser.getExtensionFilters().clear();
            fileChooser.getExtensionFilters().add(extFilter);

            File file = fileChooser.showOpenDialog(generateKeysButton.getScene().getWindow());

            if (file == null) {
                showCancelledMessage();
                return null;
            }

            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream oin = new ObjectInputStream(fis);
            Signature signature = (Signature) oin.readObject();
            fis.close();
            oin.close();

            return signature;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            showErrorMessage();
            return null;
        }
    }
}
