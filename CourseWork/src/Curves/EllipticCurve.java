package Curves;

import Fields.GaloisField;

import java.io.Serializable;
import java.math.BigInteger;

public abstract class EllipticCurve implements Serializable {

    /**
     * The field on which the curve elements are located.
     */
    GaloisField field;

    /**
     * Coefficients of the curve:
     * coefficients[0] before x^2
     * coefficients[1] before x
     * coefficients[2] before y^2
     * coefficients[3] before y
     * coefficients[4] before xy
     * coefficients[5] free term.
     */
    final BigInteger[] coefficients;

    /**
     * Constructs the curve using its coefficients.
     * @param coefficients the coefficients before elements in equatation.
     * @param field the field on which the curve elements are located.
     * @throws IllegalArgumentException if coefficients are null or not corresponding or field == null.
     */
    EllipticCurve(BigInteger[] coefficients, GaloisField field) throws IllegalArgumentException {

        if (coefficients == null || coefficients.length != 6) {
            throw new IllegalArgumentException(getClass().getName()
                    + "coefficients.length != 6 or coefficients null");
        }

        if (field == null) {
            throw new IllegalArgumentException(getClass().getName() + "field == null");
        }

        this.coefficients = coefficients;
        this.field = field;
    }

    /**
     * Constructs the curve with empty coefficients
     */
    EllipticCurve() {
        coefficients = new BigInteger[6];
    }

    /**
     * Adds two points on the curve.
     * @param P the first point to add.
     * @param Q the second point to add.
     * @return the resulting point.
     * @throws java.lang.IllegalArgumentException if {@code P == null || Q == null}.
     */
    public abstract CurvePoint sumPoints(CurvePoint P, CurvePoint Q) throws IllegalArgumentException;

    /**
     * Doubles the given point {@code P}. Works correctly?
     *
     * @param P the point to be doubled.
     * @throws java.lang.IllegalArgumentException if {@code P == null}.
     * @return doubled point.
     */
    public abstract CurvePoint doublePoint(CurvePoint P) throws IllegalArgumentException;


    /**
     * Multiplies the given point {@code P} {@code k} times.
     * @param P the given point to be multiplied.
     * @param k the given multiplier.
     * @return the result of multiplication.
     * @throws java.lang.IllegalArgumentException if {@code P == null || k <= 0}.
     */
    public abstract CurvePoint multiplyPoint(CurvePoint P, BigInteger k) throws IllegalArgumentException;

    /**
     * Negates the given point.
     * @param P the point to be negated.
     * @return new negated point
     * @throws IllegalArgumentException if {@code P == null}
     */
    public abstract CurvePoint negatePoint(CurvePoint P) throws IllegalArgumentException;

    /**
     * Gets the field of the curve.
     * @return the field.
     */
    public GaloisField getField() {
        return this.field;
    }
}
