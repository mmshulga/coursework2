package Curves;

import Fields.GaloisField;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * Class represents the elliptic curve that looks like y^2 = x^3 + ax + b;
 */
public class EllipticCurve4 extends EllipticCurve implements Serializable {

    public EllipticCurve4(BigInteger a, BigInteger b, GaloisField field) {

        BigInteger p = field.getP();

        coefficients[1] = a.mod(p);
        coefficients[5] = b.mod(p);
        this.field = field;
    }


    /**
     * Adds two points on the curve. Works correctly
     *
     * @param P the first point to add.
     * @param Q the second point to add.
     * @return the resulting point.
     * @throws java.lang.IllegalArgumentException if {@code P == null || Q == null}.
     */
    @Override
    public CurvePoint sumPoints(CurvePoint P, CurvePoint Q) throws IllegalArgumentException {

        if (P == null || Q == null) {
            throw new IllegalArgumentException(getClass().getName() + "wrong args");
        }

        if (P.isInfinity()) {
            return Q.clone();
        }

        if (Q.isInfinity()) {
            return P.clone();
        }

        //Check whether the point should be doubled or maybe it would be at infinity,
        if (P.x.compareTo(Q.x) == 0) {
            if (P.y.compareTo(Q.y) == 0) {
                return doublePoint(P.clone());
            }
            else {
                return new CurvePoint(this.field);
            }

        } else {
            BigInteger p = field.getP();

            //Numerator and denominator of lambda.
            BigInteger t1 = ((Q.y).subtract(P.y)).mod(p);
            BigInteger t2 = ((Q.x).subtract(P.x)).mod(p);

            BigInteger lambda = BigInteger.ZERO;

            //If not checked, not invertible exception is thrown.
            if (t2.compareTo(BigInteger.ZERO) != 0)
                lambda = t1.mod(p).multiply(t2.modInverse(p)).mod(p); //= t1/t2 mod p

            //INFINITY
            if (lambda.signum() == 0) {
                return new CurvePoint(field);
            }

            //Formulas.
            BigInteger xResult = (lambda.multiply(lambda).subtract(P.x).subtract(Q.x)).mod(p);
            BigInteger yResult = (lambda.multiply(P.x.subtract(xResult)).subtract(P.y)).mod(p);

            return new CurvePoint(xResult, yResult, this.field);
        }
    }

    /**
     * Doubles the given point {@code P}
     *
     * @param P the point to be doubled
     * @throws java.lang.IllegalArgumentException if {@code P == null}
     * @return doubled point
     */
    @Override
    public CurvePoint doublePoint(CurvePoint P) throws IllegalArgumentException {

        if (P == null) {
            throw new IllegalArgumentException(getClass().getName() + "wrong args");
        }
        if (P.x == null || P.y == null) return new CurvePoint(P.getField());

        BigInteger p = field.getP();
        CurvePoint Q = P.clone();

        //Numerator of lambda
        BigInteger t1 = (P.x).multiply(P.x).multiply(new BigInteger("3")).add(coefficients[1]);

        //Denominator of lambda.
        BigInteger t2 = P.y.multiply(new BigInteger("2"));

        BigInteger lambda = BigInteger.ZERO;

        //If not checked, not invertible exception is thrown.
        if (t2.compareTo(BigInteger.ZERO) != 0)
             lambda = t1.mod(p).multiply(t2.modInverse(p)).mod(p); //= t1/t2 mod p

        //INFINITY
        if (lambda.signum() == 0) {
            return new CurvePoint(field);
        }

        //Formulas.
        BigInteger xResult = (lambda.multiply(lambda).subtract((P.x).multiply(new BigInteger("2")))).mod(p);
        BigInteger yResult = (lambda.multiply(P.x.subtract(xResult)).subtract(P.y)).mod(p);

        Q.x = xResult;
        Q.y = yResult;

        return Q;
    }

    /**
     * Multiplies the given point {@code P} {@code k} times.
     * @param P the given point to be multiplied.
     * @param k the given multiplier.
     * @return the result of multiplication.
     * @throws java.lang.IllegalArgumentException if {@code P == null || k <= 0}.
     */
    @Override
    public CurvePoint multiplyPoint(CurvePoint P, BigInteger k) throws IllegalArgumentException {
        return scalarMultiplication(P, k);
    }

    /**
     * Negates the given point.
     *
     * @param P the point to be negated.
     * @return new negated point
     * @throws IllegalArgumentException if {@code P == null}
     */
    @Override
    public CurvePoint negatePoint(CurvePoint P) throws IllegalArgumentException {
        CurvePoint Q;
        BigInteger newY = P.y.negate();
        newY = newY.mod(getField().getP());

        Q = new CurvePoint(P.x, newY, field);
        return Q;
    }

    /**
     * Implements the logic for the scalar multiplication of a point P.
     * @param P the point to be multiplied.
     * @param k the number of times for the point P to be multiplied.
     * @return kP.
     * @throws IllegalArgumentException if P == null or k == null or k < 0.
     */
    private CurvePoint scalarMultiplication(CurvePoint P, BigInteger k) throws IllegalArgumentException {
        if (P == null) {
            throw new IllegalArgumentException(getClass().getName() + " wrong args!,P == NULL");
        }
        if (k == null || k.signum() < 0) {
            throw new IllegalArgumentException(getClass().getName() + " wrong args!, k < 0 or k == null");
        }

        char[] multiplier = k.toString(2).toCharArray();
        int N = multiplier.length;

        CurvePoint[] U = new CurvePoint[N];
        if (multiplier[0] == '1') {
            U[N - 1] = P.clone();
        } else {
            U[N - 1] = new CurvePoint(field);
        }

        for (int i = 1; i < N; i++) {
            if (multiplier[i] == '1') {
                U[N - 1 - i] = sumPoints(P.clone(), doublePoint(U[N - i]));
            } else {
                U[N - 1 - i] = doublePoint(U[N - i]);
            }
        }
        return U[0];
    }
}
