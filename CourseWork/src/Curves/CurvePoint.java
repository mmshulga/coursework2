package Curves;

import Fields.GaloisField;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * Class represents the point in the galois field.
 */
public class CurvePoint implements Cloneable, Serializable {

    /**
     * Field in which the point is located.
     */
    private final GaloisField field;

    /**
     * X - coordinate of a point.
     */
    BigInteger x;

    /**
     * Y - coordinate of a point.
     */
    BigInteger y;

    /**
     * Gets the field in which the point is located.
     * @return the field
     */
    public GaloisField getField() {
        return field;
    }

    /**
     * Gets the x-coordinate of the given point.
     * @return the x-coordinate.
     */
    public BigInteger getX() {
        return x;
    }

    /**
     * Gets the y-coordinate of the given point.
     * @return the y-coordinate.
     */
    public BigInteger getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CurvePoint that = (CurvePoint) o;
        if (this.isInfinity() && that.isInfinity()) return true;
        if (this.isInfinity() && !that.isInfinity()) return false;
        if (!this.isInfinity() && that.isInfinity()) return false;

        if (field.getP().compareTo(that.field.getP()) != 0) return false;
        if (x.compareTo(that.x) != 0) return false;
        return y.compareTo(that.y) == 0;

    }

    /**
     * Constructor.
     * @param x X - coordinate.
     * @param y Y - coordinate.
     * @param field - field in which the point is located.
     */
    public CurvePoint(BigInteger x, BigInteger y, GaloisField field) {
        BigInteger P = field.getP();
        this.x = x.mod(P);
        this.y = y.mod(P);
        this.field = field;
    }

    /**
     * Constructs the point using only the field. The constructed point is called "infinite point".
     * @param field the field in which the point is located.
     */
    public CurvePoint(GaloisField field) {
        this.x = null;
        this.y = null;
        this.field = field;
    }

    /**
     * Checks whether point is a neutral element.
     * @return whether point is a neutral element.
     */
    public boolean isInfinity() {
        return x == null && y == null;
    }

    /**
     * Clones the current point.
     * @return the copy of the current point.
     */
    public CurvePoint clone() {
        CurvePoint result = new CurvePoint(this.field);

        if (this.x == null || this.y == null) return result;

        result.x = new BigInteger(this.x.toString());
        result.y = new BigInteger(this.y.toString());

        return result;
    }

    @Override
    public String toString() {
        return "(" + this.x + ", " + this.y + ")";
    }
}
