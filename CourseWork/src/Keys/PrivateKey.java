package Keys;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * Represents the private key.
 */
public class PrivateKey implements Serializable {

    /**
     * The k.
     */
    private final BigInteger key;

    /**
     * Constructs the private key.
     * @param key the value for the private key.
     */
    public PrivateKey(BigInteger key) {
        this.key = key;
    }

    /**
     * Returns the k.
     * @return k.
     */
    public BigInteger getKey() {
        return key;
    }

    @Override
    public String toString() {
        return "Закрытый ключ \n" +
                "Ключ = " + key;
    }
}
