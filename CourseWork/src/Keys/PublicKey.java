package Keys;

import Curves.CurvePoint;
import Curves.EllipticCurve;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * Represents the public key.
 */
public class PublicKey implements Serializable {
    /**
     * The curve for the key.
     */
    private final EllipticCurve curve;

    /**
     * The generator of the curve.
     */
    private final CurvePoint generator;

    /**
     * The kG.
     */
    private final CurvePoint point;

    /**
     * The order of the generator.
     */
    private final BigInteger n;

    /**
     * Constructs the given public key using params.
     * @param curve the elliptic curve.
     * @param generator the generator of the curve
     * @param point the kG.
     * @param n the order of the generator.
     */
    public PublicKey(EllipticCurve curve, CurvePoint generator, CurvePoint point, BigInteger n) {
        this.curve = curve;
        this.generator = generator;
        this.point = point;
        this.n = n;
    }

    /**
     * Gets the curve.
     * @return the curve.
     */
    public EllipticCurve getCurve() {
        return curve;
    }

    /**
     * Gets the generator of the curve.
     * @return the generator.
     */
    public CurvePoint getGenerator() {
        return generator;
    }

    /**
     * Gets the order of the generator.
     * @return n.
     */
    public BigInteger getN() {
        return n;
    }

    /**
     * Gets the kG
     * @return the point.

     */
    public CurvePoint getPoint() {
        return point;
    }

    @Override
    public String toString() {
        return "Открытый ключ\n" +
                " Генератор = " + generator +
                "\nТочка = " + point;
    }
}
