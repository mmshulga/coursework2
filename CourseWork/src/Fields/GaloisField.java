package Fields;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * Class representing Galois field.
 */
public class GaloisField implements Serializable {

    /**
     * The order of a field.
     */
    private final BigInteger P;

    /**
     * Accessor method for P.
     * @return this.P .
     */
    public BigInteger getP() {
        return P;
    }

    /**
     * Constructor.
     * @param p the order of a field.
     * @throws IllegalArgumentException {@code p <= 0}.
     */
    public GaloisField(BigInteger p) throws IllegalArgumentException {

        if (p.signum() <= 0) {
            throw new IllegalArgumentException(getClass().getName()
                    + " precondition violated!");
        }

        this.P = p;
    }
}
